# Reto 2: Consultas básicas

En este reto trabajamos con dos bases de datos por separado: `empresa` y `videoclub`, que nos viene dado en el fichero `empresa.sql` y `videoclub.sql` respectivamente. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C1/reto2_consultas%20b%C3%A1sicas

# EMPRESA

## Query 1
**Muestre los productos (código y descripción) que comercializa la empresa.**  
Para esta consulta no hay mucho misterio, simplemente es una consulta básica que en este caso podemos hacerla de dos maneras diferentes. Como todos los campos son los que piden pues podemos utilizar `*` o especificar los campos. 
```sql
--query 1
SELECT P.*
FROM PRODUCTE AS P;
```
```sql
--query 1
SELECT P.PROD_NUM, P.DESCRIPCIO
FROM PRODUCTE AS P;
```
## Query 2
**Muestre los productos (código y descripción) que contienen la palabra tennis en la descripción.**  
Como hemos visto anteriormente podemos utilizar `LIKE`, ya que se usa para comparar carácteres donde podamos utilizar carácteres comodín en algún campo.
```sql
--query 2
SELECT *
FROM PRODUCTE AS P
WHERE P.DESCRIPCIO LIKE '%TENNIS%';
```
## Query 3
**Muestre el código, nombre, área y teléfono de los clientes de la empresa.**  
Esta consulta es parecedia a la consulta [*query 1*](#query-1), pero en este caso no podemos usar el `*` porque el enunciado nos pide exclusivamente los campos: `CLIENT_COD`, `NOM`, `AREA`y `TELEFON`.
```sql
--query 3
SELECT C.CLIENT_COD, C.NOM, C.AREA, C.TELEFON
FROM CLIENT AS C;
```
## Query 4
**Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.**  
Podemos reutilizar el esqueleto de la [*query 3*](#query-3) para realizar esta consulta. Simplemente hay que especificar que el `AREA` sea diferente a 636 con un operador lógico `!=` como se pide en el enunciado.
```sql
--query 4
SELECT C.CLIENT_COD, C.NOM, C.AREA, C.CIUTAT, C.AREA
FROM CLIENT AS C
WHERE C.AREA != 636;
```
imprimo el area para comprobar
## Query 5
**Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).**  
Sin diferir mucho en las anteriores consultas se nos pide una consulta sencilla donde se piden un serie de campos concretos de una tabla, es este caso se busca las columnas: `COM_NUM`, `COM_DATA` y `DATA_TRAMESA` de la tabla `COMANDA`
```sql
--query 5
SELECT C.COM_NUM, C.COM_DATA, C.DATA_TRAMESA 
FROM COMANDA AS C;
```

# VIDEOCLUB

## Query 6
**Lista de nombres y teléfonos de los clientes.**  
De nuevo esta consulta no tiene mucha dificultad ya que se piden unos campos concretos de una tabla concreta, en este caso: `Nom` y `Telefon` de la tabla `CLIENT`. En este caso he especificado la base de datos para evitar probelmas ya que la palabra `CLIENT` es una palabra reservada de SQL.
```sql
--query 6
SELECT C.Nom, C.telefon
FROM Videoclub.CLIENT AS C;
```
## Query 7
**Lista de fechas e importes de las facturas.**  
Otra consulta sencilla sin mucha complicación, en este caso las palabras: `Data` y `Import` son palabras reserbadas así que es aconsejable concretar los datos lo mejor posible, en este caso especifico la tabla.
```sql
--query 7
SELECT F.Data, F.Import
FROM FACTURA AS F;
```
## Query 8
**Lista de productos (descripción) facturados en la factura número 3.**  
A diferencia de los anteriores en esta consulta se pide que un campo sea más concreto y no imprimir todos los campos. Para este ejericio se pide que el `CodiFactura` sea igual a 3 y para eso utilizamos `=`.
```sql
--query 8
SELECT D.Descripcio
FROM DETALLFACTURA AS D
WHERE D.CodiFactura = 3;
```
## Query 9
**Lista de facturas ordenada de forma decreciente por importe.**  
En esta consulta se nos pide mostrar el output de forma más ordenada, en concreto se nos pide que lo mostremos de forma decreciente a partid del `Import` de las facturas.
```sql
--query 9
SELECT F.*
FROM FACTURA AS F
ORDER BY F.Import DESC;
```
## Query 10
**Lista de los actores cuyo nombre comience por X.**  
Como hemos visto en la [*query 2*](#query-2) podemos utilizar `LIKE` para este ejercicio ya que buscamos un actor que empiece por una letra en específico.
```sql
--query 10
SELECT A.*
FROM ACTOR AS A
WHERE A.Nom LIKE 'X%';
```

# Adicional

A partir de aquí son querys adicionales que hemos realizado y explicado en clase. Lo curiosos es que ya no son todas `SELECT` sino que tenemos también inserciones y borrado con `INSERT INTO` y `DELETE FROM` respectivamente. TAmbién encontramos una query ([*query 15*](#query-15)) que se nos presenta el `UPDATE` que sirve para modificar datos sin borrarlos. 

# VIDEOCLUB

## Query 11
**Añade dos actores que empizan por la letra X.**  
Esta query no había aparecido anteriormente. Básicamente lo que hace el `INSERT INTO` es insertar campos en una tabla en concreto. De hecho lo hemos visto con otros profesores pero para insertar campos en tablas a través de querys que nos generaba *ChatGPT*<sub>[[1](#referencias)]</sub>.
```sql
--query 11
INSERT INTO ACTOR
VALUES (4, "Ximo");
```
```sql
--query 11
INSERT INTO ACTOR
VALUES (5, "Xavi");
```
Se puede alterar el orden de las columnas de la siguiente manera:
```sql
--query 11
INSERT INTO ACTOR (Nom, CodiActor)
VALUES ("Xordi", 6);
```
Especificar los campos puede servir para insertar en la base de datos campos sin tener que poner otros (Por ejemplo insertar solo el CodiActor sin nombre o meter facturas donde no se conoce el DNI).  

**Comprueba los valores añadidos.**  
Básicamente es la [*query 10*](#query-10) copiada.
```sql
--query 11.5
SELECT A.*
FROM ACTOR AS A
WHERE A.Nom LIKE 'X%';
```

## Query 12
**Elimina el actor "Xordi".**
Para eliminar registros de una tabla podemos utilizar `DELETE FROM` como se muestra en la siguiente query. Se usa el código para evitar el modo seguro que no deja borrar un registro sin la clave.
```sql
--query 12
DELETE FROM ACTOR AS A
WHERE A.CodiActor = 7; --Xordi tiene código 7
``` 
## query 13
**Elimina a varios actores.**  
Si se quieren eliminar varios al mismo tiempo tenemos dos opciones:
```sql
--query 13
DELETE FROM ACTOR AS A
WHERE A.CodiActor = 4 OR A.CodiActor = 5;
```
Hay un sinónimo de `OR` que es `IN`:
```sql
--query 13
DELETE FROM ACTOR AS A
WHERE A.CodiActor IN (4, 5);
```

## Quary 14
**Añade 2 actores a la vez**  
Para añadir varios campos a una tabla no es necesario hacer dos `INSERT INTO`, podemos anidarlos de la siguiente forma:
```sql
--query 14
INSERT INTO ACTOR (Nom, CodiActor)
VALUES ("Xordi", 6), ("Xavi", 7);
```

## Query 15
**Cambiale el nombre a Xordi por Jordi sin eliminarlo.**  
Como se ha mencionado al principio de la sección [*Adicional*](#adicional) (en el resumen) que se ha utilizado `UPDATE` en esta query para modificar el dato sin tener que borrarlo, en este caso se modifica el `Nom` de "Xordi" a "Jordi". Se usa el código para evitar el modo seguro que se ha mencionado anteriormente que no deja modificar un registro sin la clave.
```sql
--query 15
UPDATE ACTOR AS A
SET A.Nom = "Jordi"
WHERE A.CodiActor = 6; --Xordi tiene código 6
```

# EMPRESA

## Query 16
**¿En qué años hemos dado de alta trabajadores?**  
En esta consulta se nos presenta una nueva función. `DISTINC` es simplemente para evitar output repetido, en este caso se ha usado porque hay años en los que se han dado de alta varios trabajdores.
```sql
--query 16
SELECT DISTINCT YEAR(E.DATA_ALTA)
FROM EMP AS E;
```

## Query 17
**¿Qué categorias de empleados tengo actualmente?**  
Aquí se vuelve a utlizar `DISTINC` para ver el total de `OFICIS` que tenemos en la tabla de `EMP`.
```sql
--query 17
SELECT DISTINCT E.OFICI
FROM EMP AS E;
```
**¿Cuántas son en total?**  
Para contar el número de registros que puede dar una query se puede usar `COUNT()` que básicamente hace lo que promete, cuenta.
```sql
--query 17.5
SELECT COUNT(DISTINCT E.OFICI)
FROM EMP AS E;
```

## Query 18
**¿Qué empleados van a comisión?**  
En este caso en clase hemos mencionado la posibilidad de usar `!=` pero como el "valor" `NULL` no se considera como tal un valor no puede hacer una comprobación, así que para este caso debemos utilizar `IS NOT` para negar y `IS` para lo contrario de la anterior. 
```sql
--query 18
SELECT E.*
FROM EMP AS E
WHERE E.COMISSIO IS NOT NULL;
```

## Query 19
**¿Qué dos empleados se llevan la mayor comisión?**  
En este caso se nos introduce el `LIMIT`, lo que hace es limitar el número de búsquedas, es decr si hay por ejemplo 10 y quieres que solo salgan 5 puedes utilizarlo. En este caso lo hemos utilizado con `ORDER BY` para poder obtener los dos empleados con mayor comisión.
```sql
--query 19
SELECT E.*
FROM EMP AS E
WHERE E.COMISSIO IS NOT NULL
ORDER BY E.COMISSIO DESC
LIMIT 2;
```

## REFERENCIAS
[1] [Link de la herramienta ChatGPT](https://chat.openai.com/)