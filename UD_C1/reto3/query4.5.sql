--query 4.5
SELECT P.Titol AS "Título de la película", A.Nom AS "Actor"
FROM PELICULA AS P 
JOIN INTERPRETADA AS I 
ON P.CodiPeli = I.CodiPeli
JOIN ACTOR AS A
ON I.CodiActor = A.codiActor