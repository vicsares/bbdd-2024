--query 1
SELECT P.CodiPeli, P.Titol, G.Descripcio
FROM PELICULA AS P 
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;