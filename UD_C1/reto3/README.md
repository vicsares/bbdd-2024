# Reto 3: JOIN

En este reto trabajamos con una base de datos: `videoclub`, que nos viene dado en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedido en cada uno de los enunciados.  
En este caso vamos a utilizar una operación explicada en clase llamada `JOIN`. Esta operación es fundamental para el modelo relacional de bases de datos para recuperar información de varias tablas. Podemos encontrar varios tipos de `JOIN`s:  

***
Teniendo en cuenta estas dos tablas:   

**PELÍCULAS:**
| ID | Nombre        | Género |
|----|---------------|--------|
| 1  | Pulp Fiction  | NULL   |
| 9  | Life of Brian | 2      |
| 8  | La máscara    | 2      |

**GÉNEROS:**
| ID | Nombre  |
|----|---------|
| 1  | Terror  |
| 2  | Comedia |
| 3  | Drama   |
***

- **INNER JOIN**: Devuelve solo las filas que tienen coincidencias en ambas tablas.
```sql
SELECT *
FROM PELICULA AS P 
INNER JOIN GENERE AS G --Mysql entiende JOIN como INNER JOIN
ON P.CodiGenere = G.CodiGenere;

```
| PELÍCULAS    | GÉNERO    |
| ------------- | ------------- |
| <table><tr><td>**ID**</td><td>**Nombre**</td><td>**Género**</td></tr><tr><td>9</td><td>Life of Brian</td><td>2</td></tr></tr><tr><td>8</td><td>La máscara</td><td>2</td></tr></tr></table> | <table><tr><td>**ID**</td><td>**Nombre**</td></tr><tr><td>2</td><td>Comedia</td></tr><tr><td>2</td><td>Comedia</td></tr></table> |

- **LEFT JOIN**: Devuelve todas las filas de la tabla de la izquierda y las filas coincidentes de la tabla de la derecha, si las hay. Si no hay coincidencias, devuelve NULL para las columnas de la tabla de la derecha.
```sql
SELECT *
FROM PELICULA AS P 
LEFT JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```
| PELÍCULAS    | GÉNERO    |
| ------------- | ------------- |
| <table><tr><td>**ID**</td><td>**Nombre**</td><td>**Género**</td></tr><tr><td>1</td><td>Pulp Fiction</td><td>NULL</td></tr></tr><tr><td>9</td><td>Life of Brian</td><td>2</td></tr></tr><tr><td>8</td><td>La máscara</td><td>2</td></tr></table> | <table><tr><td>**ID**</td><td>**Nombre**</td></tr><tr><td>---</td><td>---</td></tr></tr><tr><td>2</td><td>Comedia</td></tr></tr><tr><td>2</td><td>Comedia</td></tr></table> |

- **RIGHT JOIN**: Es similar al LEFT JOIN, pero devuelve todas las filas de la tabla de la derecha y las filas coincidentes de la tabla de la izquierda.
```sql
SELECT *
FROM PELICULA AS P 
RIGHT JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```
| PELÍCULAS    | GÉNERO    |
| ------------- | ------------- |
| <table><tr><td>**ID**</td><td>**Nombre**</td><td>**Género**</td></tr><tr><td>9</td><td>Life of Brian</td><td>2</td></tr></tr><tr></tr></tr><tr><td>8</td><td>La máscara</td><td>2</td></tr><tr><td>---</td><td>---</td><td>---</td></tr></table> | <table><tr><td>**ID**</td><td>**Nombre**</td></tr></tr><tr><td>2</td><td>Comedia</td></tr></tr><tr><td>2</td><td>Comedia</td></tr><tr><td>1</td><td>Terror</td></tr></table> |

_imagen sacada de la web: https://www.bigbaydata.com/tipos-de-join-en-sql/_
![Imagen de los JOINs de Mysql en gráfico](https://www.bigbaydata.com/wp-content/uploads/2023/03/sqljoins.jpg)

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C1/reto3?ref_type=heads

## Query 1
**Lista todas las películas del videoclub junto al nombre de su género.**  
Para esta query hemos empezado a utilizar los `JOIN`s explicados anteriormente. Como se ha comentado antes en _Mysql_ si no se especifica un tipo de `JOIN` en concreto este lo entiende como un `INNER JOIN`.
```sql
--query 1
SELECT P.CodiPeli, P.Titol, G.Descripcio
FROM PELICULA AS P 
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```
## Query 2
**Lista todas las facturas de María.**  
Para ver las facturas de María necesitamos dos tablas. La tabla de `FACTURA` y la tabla de `CLIENT` donde podemos encontrar todas las faturas y todos los clientes. Para ello necesitamos relacionarla con un `JOIN`, en este caso hemos usado un `INNER JOIN` porque 
```sql
--query 2
SELECT C.Nom, F.*
FROM FACTURA AS F 
JOIN CLIENT AS C
ON F.DNI = C.DNI
WHERE C.Nom LIKE "Maria%";
```
## Query 3
**Lista las películas junto a su actor principal.**  
Para esta query se busca listar las películas justo a su actor principal. Para ello necesitamos acceder a dos tablas: `PELICULA` y `ACTOR` para podemos mostrar el nombre del actor y el de la película. Para ello hemos usado un `INNER JOIN`.
```sql
--query 3
SELECT P.Titol, A.Nom
FROM PELICULA AS P 
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;
```
## Query 4
**Lista películas junto a todos los actores que la interpretaron.**  
En este caso hemos hecho un doble `INNER JOIN` ya que buscamos listar todos los actores de todas las películas. Para ello necesitamos utilizar una tabla auxiliar `INTERPRETADA` que relaciona las tablas `ACTOR` y `PELICULA`. La sintaxis de realizar varios `JOIN`s se puede hacer de dos formas diferentes:
```sql
--query 4
SELECT P.Titol AS "Título de la película", A.Nom AS "Actor"
FROM PELICULA AS P 
JOIN INTERPRETADA AS I 
JOIN ACTOR AS A
ON P.CodiPeli = I.CodiPeli AND I.CodiActor = A.CodiActor;
```
También se puede realizar un `JOIN` y hacerle un `JOIN` al resultado del primero:
```sql
--query 4.5
SELECT P.Titol AS "Título de la película", A.Nom AS "Actor"
FROM PELICULA AS P 
JOIN INTERPRETADA AS I 
ON P.CodiPeli = I.CodiPeli
JOIN ACTOR AS A
ON I.CodiActor = A.codiActor
```
## No entra en el examen (SELF JOIN)
## Query 5
**Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.**  
Un `SELF JOIN` es un join pegada a si mismo. Estos `JOIN`s sirven para cuando una tabla está relaciana consigo misma (Ejemplo: películas con segunda parte, clase con delegado, etc...). Es **obligatorio** usar alias (`AS`) para poder identificar cada una de las tablas.
```sql
--query 5
SELECT P1.CodiPeli AS "Código", 
       P1.Titol AS "Título", 
       P2.CodiPeli AS "Código segunda película", 
       P2.Titol AS "Segunda parte"
FROM PELICULA AS P1
JOIN PELICULA AS P2
ON P1.CodiPeli = P2.SegonaPart;
```