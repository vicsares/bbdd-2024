--query 4
SELECT P.Titol AS "Título de la película", A.Nom AS "Actor"
FROM PELICULA AS P 
JOIN INTERPRETADA AS I 
JOIN ACTOR AS A
ON P.CodiPeli = I.CodiPeli AND I.CodiActor = A.CodiActor;