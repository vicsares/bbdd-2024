# Pasos a seguir para conectarse
## 1º Paso
Primero hay que conectarse a la base de datos del profesor con el comando que vimos en clase, se puede hacer desde la consola o desde el workbench:
```bash
mysql -h 192.168.4.199 -P 33061 -u Manolo -p
```
Siendo '-h' el host '-P' el puerto y '-u' el usuario a conectarse.

## 2º Paso
Lo primero que hay que hacer cuando estamos conectados es ver las bases de datos que tenemos disponibles con ese usuario:
```sql
SHOW DATABASES;
```
Cuando ya tengamos la base de datos localizada habría que utilizarla con un `USE`:
```sql
USE DB;
```

## 3º Paso
A continuación lo que habría que hacer sería ver los permisos de ese usuario con `GRANTS` para ver si tenemos algún rol:
```sql
SHOW GRANTS;
```
O también se pueden listar los roles de la cuenta actual haciendo un `SELECT` de `CURRENT_ROLE();` de la siguiente manera:
```sql
SELECT CURRENT_ROLE();
```

## 4º Paso
Si detectamos que tenemos un rol habría que activarlo, podemos hacerlo de dos formas diferentes:
```sql
SET ROLE 'rol encontrado';
```
O si queremos asignarle el rol por defecto (Que ya lo tenga cuando iniciemos sesión):
```sql
SET DEFAULT ROLE 'rol encontrado'  TO 'Manolo'@'192.168.4.199';
```

## 5º Paso
Ver las columnas que tenemos disponibles en la base de datos (Aunque seguramente ya se reflejen en los permisos):
```sql
SHOW TABLES;
```

## 6º Paso
Leer muy bien las preguntas que nos pongan, ya que si nos preguntan algo y un usuario solo puede ver la mitad de registros entonces no estaríamos contestanto bien.  
Para ello iniciaremos sesión en el usuario con más privilegios o con todos los usuarios disponibles para hacer las consultas.

## Recuerda
Hay un atajo de teclado para poder utilizar los comandos antiguos con:  
<kbd>control</kbd>+<kbd>r</kbd> para hacer como una especie de búsqueda entre los anteriores comandos.

## Posibles preguntas de teoría
### Reto 3.1
#### Respuesta a las preguntas
**¿Por qué necesitamos tres tablas?**  
Necesitamos tres tablas ya que como podemos ver en el diseño de la base de datos las tablas *pasajeros* y *vuelos* estás relacionadas de muchos a muchos, esto requiere una tabla externa como es la tabla *reservas*.

**¿Cuáles son las claves primarias y foráneas?**  
Las claves primarias en una tabla son una forma de identificar cada registro de la tabla, los valores del campo donde esté asignada la clave no se pueden repetir. Por otro lado una clave foránea es una clave primaria de otra tabla, se usa para relacionar los datos y evitar datos erroneos como (en este caso) vuelos o pasajeros que no existan.

**¿Por qué utilizamos las restricciones que hemos definido y no otras?**  
Hemos utilizado las restricciones `NOT NULL` para evitar valores nulos y que sea necesario rellenar esos campos. Tambíen hemos utilizado `UNSIGNED` en la tabla vuelos por optimización, esto hace que el no tome valores negativos sino que se pasen a positivo.

#### Reflexiones
Para cancelar un vuelo sería necesario borrarlo de la siguiente manera:
```sql
DELETE FROM Vuelos WHERE id_Vuelo LIKE "VUELO1";
```
En este caso intentamos borrar el vuelo con id *VUELO1*. Sin embargo nos salta un error (Error: *1451*<sub>[[3](#referencias)]</sub>) que nos dice que no se puede eliminar el regitro ya que se está utilizando los datos de este registro en otra tabla (*reservas*). Si queremos borrar el registro debemos asegurarnos de que no se usa nada de información en tablas exteriores, en este caso sería borrar las reservas de ese vuelo. Se pueden denotar las ventajas e inconvenientes de las bases de datos relacionales: En primer lugar vemos que gracias a las bases de datos relacionales al relacionar la información evitamos inconcluencias (**INTEGRIDAD**) en los datos almacenados y facilitamos la búsqueda de registros entre tablas. Por otro lado nos restringe a la hora de modificar o eliminar registros, ya que no nos deja eliminar un registro si está en uso en otra tabla a través de una clave foránea. 

### Reto 3.2
Para bloquear una cuenta podemos usar `ALTER USER` con la especificación `ACCOUNT LOCK` (`UNLOCK` para desbloquear):
```sql
ALTER USER "ivan" ACCOUNT LOCK;
```
Sin embargo no se desconecta a los usuarios que ya están conectados, para ellos podemos hacelo de la siguiente manera:
```sql
SELECT id FROM information_schema.processlist WHERE user = 'ivan';
```
Se verán los procesos que están corriendo como 'ivan'. Se pueden eliminar con `KILL`:
```sql
KILL 73;
```
donde '73' es el número del proceso activo.

#### Servicios
Los servicios en linux se les suele llamar "deamon" (o service), un ejemplo es el nombre de servicio ssh que se suele llamar sshd (Secure Shell Deamon), es decir que se le pone una 'd' al final. En linux el gestor de servicios se llama "systemctl", es un servicio que controla servicios.
Se pueden ver los servicios con `services` y pararlos con `service mysql stop` seguido de la constraseña. Se puede ver el estado de un servicio con `service mysql status`

### Reto 2.3
#### Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa.
```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```
#### Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`.  
#### Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento.

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa.

La normalización de bases de datos es un proceso que consiste en designar y aplicar una serie de reglas a las relaciones obtenidas tras el paso del modelo entidad-relación al modelo relacional.

### Roles
En MySQL, puedes asignar varios roles a un usuario al activarlos en una sesión de conexión. Sin embargo, para poder hacer esto, MySQL debe haber sido configurado para utilizar roles. A partir de MySQL 8.0.16, los roles son compatibles, pero su uso puede requerir algunas configuraciones previas.

Aquí tienes los pasos generales para activar varios roles en MySQL:

### 1. Crear los roles necesarios:
Puedes crear los roles utilizando la sentencia `CREATE ROLE`. Por ejemplo:
```sql
CREATE ROLE role1;
CREATE ROLE role2;
```

### 2. Asignar privilegios a los roles:
Una vez que hayas creado los roles, puedes asignarles privilegios utilizando las sentencias `GRANT`. Por ejemplo:
```sql
GRANT SELECT ON database1.* TO role1;
GRANT INSERT ON database2.* TO role2;
```

### 3. Asignar roles a un usuario:
Puedes asignar uno o varios roles a un usuario utilizando la sentencia `GRANT`. Por ejemplo:
```sql
GRANT role1, role2 TO 'usuario'@'localhost';
```

### 4. Activar los roles en la sesión:
Una vez que un usuario ha iniciado sesión, puede activar los roles asignados utilizando la sentencia `SET ROLE`. Por ejemplo:
```sql
SET ROLE role1, role2;
```

### Notas importantes:
- Debes asegurarte de tener los privilegios necesarios para crear y asignar roles.
- Es importante que tu versión de MySQL sea compatible con roles. A partir de MySQL 8.0.16, los roles son compatibles, pero en versiones anteriores podrían no estar disponibles o tener limitaciones.
- También debes considerar la gestión adecuada de los roles y los privilegios para mantener la seguridad de tu base de datos.

Recuerda que la sintaxis exacta puede variar según la versión específica de MySQL que estés utilizando, así que te recomiendo revisar la documentación oficial de MySQL para obtener más detalles sobre el uso de roles en tu versión particular.
