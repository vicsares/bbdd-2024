# Reto 3.2 DCL Control de acceso

Se van a ver los siguientes puntos sobre el control de acceso a una base de datos mysql:

    - Cómo registrar nuevos usuarios, modificarlos y eliminarlos.
    - Cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD.
    - Cómo mostrar los usuarios existentes y sus permisos.
    - Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.
    - Qué permisos hacen falta para gestionar los usuarios y sus permisos.
    - Posibilidad de agrupar los usuarios (grupos/roles/...).
    - Qué comandos usamos para gestionar los usuarios y sus permisos.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C3/Reto%203.2?ref_type=heads

## Conexión

Hay que usar el "cliente" mysql para poder gestionar los permisos, los comandos que nos ofrece la web se pueden insertar en la consola o en el workbench.
En nuestro caso hemos decidido usar la consola para practicar. Se ha tenido que usar el siguiente comando para poder acceder:
```bash
mysql -P 33006 -u root -p
```
Y después especificar la contraseña. En mi caso no ha hecho falta especificar el puerto ya que toma 'localhost' como IP a la que conectarse. Sin embargo en algunos casos de clase se ha tenido que especificar el host con `-h` a 127.0.0.1 ya que a pesar de ser lo mismo es posible que no tomara bien.

## Mostrar Datos

Al usar el comando:
```sql
SHOW DATABASES;
```
Hemos visto que hay una en específico que se llama `mysql`, es ahí donde se gestionan los usuraios, se añaden, etc...

Se pueden sacar las tablas como `CSV` para hojas de cálculo, y tambíen se pueden usar en nuestras apps. 
Con el usuario root se puede tener acceso a toda la configuración.
Para poder ver los usuarios del sistema se puede usar:
```sql
SELECT user FROM mysql.user;
```
Para ver las columnas de esta tabla usamos:
```sql
SHOW COLUMNS FROM mysql.user;
```
En el ejemplo de clase vemos en clase vemos dos tipos de localhost: `localhost` como tal que indica el propio equipo con su IP y '%' que en este caso indica que podemos acceder desde cualquier conexión.  
<hr>

    HAY QUE TENER (Con Chinook):
    crear usuario y asignar privilegios para Chinook ver la tablas concretas sin poder modificar pero la de playlist sí. Podemos añadir permisos al usuario en el host concreto. Añadir permisos a nivel de grupos de usuarios (utilizando patrones ('%', 'admin_')). Lo más interesante son los roles, a los roles se les pueden añadir privilegios (rola admin, rol usuario, etc...) y a los usuarios que se registren se les asigna el rol de usuario. 
    Interesante:
    Quitarle permisos a un usuario (`REVOKE`), Qué pasa si se renombra un usuario o como actualizar la contra de un usuario (`ALTER USER`). Con `REVOKE ALL` se eliminan todos los privilegios y con `GRANT ALL` se le asignan.
    Hay que poner tipos de autenticación a parte de una contraseña(token, etc..).
    Bloquear usuarios (`ALTER USER lock` para bloquearlo o `unlock` para desbloquear (no verificado)).

## Configuración

En mysql el control de acceso es muy configurable ya que al poder añadir usuarios y poder configurar el acceso a cada tabla para cada usuario y el tipo de permisos que se le pueden atribuir a un usuario.

Para crear un usuario en `mysql` podemos usar `CREATE USER` de la siguiente manera:
```sql
CREATE USER 'david'@'198.51.100.0/255.255.255.0';
```
Podemos ver que como es mysql hay que especificar no solo el nombre del usuario sino tambine el host de donde se conecta a la base de datos.  
Si queremos crear un usuario desde una red local para conectarse a la base de datos podemos usar `%` de la siguiente manera (en este caso):

![Esquema realizado en clase sobre la red](esquema.png)

```sql
CREATE USER 'esteban quito'@'192.168.%' IDENTIFIED BY '1234';
```
Para cambiar el host de un usuario se puede usar `RENAME USER`:
```sql
RENAME USER 'esteban quito'@'192.168.%' TO 'esteban quito'@'%';
```
De esta forma se puede acceder a 'esteban quito' desde cualquier parte del host.

## Privilegios

Podemos usar `SHOW PRIVILEGES` podemos ver los provilegios que existen en mysql. Hay dos tipos de privilegios que existen: 'dinámicos' y 'estáticos'. Vamos a trabajar con los estáticos ya que los dinámicos se utilizan en momento de ejecución, los estáticos nos permiten leer tablas, no podamos leer tablas, se puedan hacer selects, etc... Se pueden ver en https://dev.mysql.com/doc/refman/8.0/en/privileges-provided.html

Para ver los privilegios de cada usuarios accedemos recurrimos a las tablas `GRANT`(significa 'acceder' en inglés). Se puede usar el comando ´SHOW GRANTS´ de la siguiente manera:
```sql
SHOW GRANTS FOR 'root'@'localhost';
```
**Pecularidad**: en mysql los usuarios vienen identificados con el nombre de usuarios y el host de donde venga como se puede ver en el comando de arriba. Son como usuarios ya que pueden restringir o garántizar el acceso a diferentes hosts.
Se puede apreciar que nos lo separa ya que los primeros son los privilegios estáticos y los siguiente los dinámicos.

Al crear un usuario no se le asigna ningún privilegio, podemos hacerlo de la siguiente manera:

```sql
CREATE USER 'finley'@'localhost'
  IDENTIFIED BY 'password';
GRANT ALL
  ON *.*
  TO 'finley'@'localhost'
  WITH GRANT OPTION;
``` 
En este ejemplo podemos ver como se crea un usuarios desde el `localhost` llamado `findley` y con constraseña 'password'. También vemos como con `GRAND` podemos asignarle los permisos que buscamos darle, en este caso le damos todos los privilegios con `ALL` (Aquí es donde se ponen los tipos de permisos). cuando nos referimos a `ON *.*` quiere decir todas las bases de datos, podemos especificar también la base de datos en específico de la siguiente manera: `ON bankaccount.*`, siendo '*' todas las tablas de la base de datos. Desgraciadamente no podemos seleccionar varias tablas en el mismo comando así que hay que ponerlas una mano a mano.
 
Para eliminar los permisos se puede usar `REVOKE` de la siguiente manera:
```sql
REVOKE ALL
  ON *.*
  TO 'finley'@'%.example.com';
```
Como tiene la misma sintaxis qye `GRANT` podemos seguir la misma metodología anterior.

## Rol

Podemos crear roles para facilitar la integración de permisos a los nuevos usuarios.
Para crear un rol usamos `CREATE ROLE`:
```sql
CREATE ROLE 'usuario raso';
```
Se le pueden asignar premisos a los roles de la misma manera que a un usuario:
```sql
GRANT SELECT
  ON Chinook.*
  TO 'usuario raso';
```
Para añadir un usuario a un rol se hace con `GRANT` de la siguiente manera:
```sql
GRANT 'usuario raso' 
  TO 'esteban quito'@'192.168.%';
```
Siendo el primer campo el rol que queremos asignar (en este caso 'usuario raso') y el segundo campo el usuario seguido de su host (en este caso 'esteban quito'@'192.168.%').  
Posteriormente hay que activarlo entrando en la cuenta de 'esteban quito' con `SET ROLE` de la siguiente manera ('NONE' para quitar el rol):
```sql
SET ROLE 'usuario raso';
```
Cuando un usuario se loguee no se le asigna ningún rol. Para solucionar este inconveniente podemos ponerle un rol como default con `SET ROLE DEFAULT` de la siguiente manera:
```sql
SET DEFAULT ROLE 'usuario raso'  TO 'esteban quito'@'192.168.%';
```
(Después hay que activarlo con `SET ROLE` visto anteriormente).

Se pueden listar los roles de la cuenta actual haciendo un `SELECT` de `CURRENT_ROLE();` de la siguiente manera:
```sql
SELECT CURRENT_ROLE();
```

Para bloquear una cuenta podemos usar `ALTER USER` con la especificación `ACCOUNT LOCK` (`UNLOCK` para desbloquear):
```sql
ALTER USER "ivan" ACCOUNT LOCK;
```
Sin embargo no se desconecta a los usuarios que ya están conectados, para ellos podemos hacelo de la siguiente manera:
```sql
SELECT id FROM information_schema.processlist WHERE user = 'ivan';
```
Se verán los procesos que están corriendo como 'ivan'. Se pueden eliminar con `KILL`:
```sql
KILL 73;
```
donde '73' es el número del proceso activo.

## Servicios

Los servicios en linux se les suele llamar "deamon" (o service), un ejemplo es el nombre de servicio ssh que se suele llamar sshd (Secure Shell Deamon), es decir que se le pone una 'd' al final. En linux el gestor de servicios se llama "systemctl", es un servicio que controla servicios.
Se pueden ver los servicios con `services` y pararlos con `service mysql stop` seguido de la constraseña. Se puede ver el estado de un servicio con `service mysql status`

## ShortCut

<kbd>control</kbd>+<kbd>r</kbd> para hacer como una especie de búsqueda entre los anteriores comandos.

## Enlaces interés

https://www.hostinger.es/tutoriales/como-crear-usuario-mysql
https://dev.mysql.com/doc/refman/8.3/en/access-control.html
