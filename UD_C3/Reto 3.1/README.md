# Definición de datos en MySQL
En esta práctica vamos a documentar el proceso de creación de la base de datos `iberian.sql` que podemos encontrar en esta carpeta.  
En primer lugar vamos a crear la base de datos seguido de las tablas con sus columnas y los respectivos tipos de datos. También vamos a ver de forma gráfica (y a través de `SQL`) restricciones de ciertas columnas (una relación se considera una restricción con una clave ajena). Hemos insertado datos teniendo en cuenta el orden de inserción por la integridad y finalmente al eliminar y actualizar datos de las tablas nos hemos topado con ciertos problemas que solucionamos en el momento.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C3/Reto%203.1?ref_type=heads

### Crear Estructura
Con `CREATE SCHEMA` podemos **crear una base de datos**.
```sql
CREATE SCHEMA `Iberian`;
```

Podemos **crear una tabla** de la siguiente manera.
```sql
CREATE TABLE `Iberian`.`Vuelos` (
  `id_Vuelo` VARCHAR(6) NOT NULL,
  `origen` VARCHAR(45) NOT NULL,
  `destino` VARCHAR(45) NOT NULL,
  `fecha` DATETIME NOT NULL,
  `capacidad` SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id_Vuelo`));
```
```sql
CREATE TABLE `Iberian`.`Pasajeros` (
  `pasaporte` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(500) NULL,
  PRIMARY KEY (`pasaporte`));
```
```sql
CREATE TABLE `Iberian`.`Reservas` (
  `id_Reserva` VARCHAR(10) NOT NULL,
  `pasaporte` VARCHAR(45) NOT NULL,
  `id_vuelo` VARCHAR(6) NOT NULL,
  `asiento` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`id_Reserva`));
```
Como podemos ver en los tres ejemplos anteriores se usa la sentencia `CREATE TABLE` seguido del nombre de la base de datos y el nombre de la tabla. Dentro de los paréntesis vamos a especificar cada colunma de la tabla en específico de la siguiente manera:
```sql
  `nombre_tabla` tipo_dato extras,
```
Se puede observar repetidamente el tipo de dato `VARCHAR(x)`, este tipo de dato es una cade da texto, tambíen vemos `DATETIME` que su nombre ya describe que se utiliza para fechas y por último vemos un `SMALLINT` que un número entero entre -32768 y 32767, sin embargo, si es `UNSIGNED` (que significa que no puede ser negativo) puede ser entre 0 y 65535 (el doble) (información sacada de la web de *MySQL*<sub>[[2](#referencias)]</sub>). Uno de los extra que podemos ver es `NOT NULL`, esto quiere decir que el campo no puede ser nulo. Para concluir podemos ver como se pueden asignar las claves primarias con la consigna `PRIMARY KEY` de la tabla seguido de la columna que se quiere entre paréntesis.

### Alterar la base de datos
Se puede **alterar la tabla** para añadir las claves ajenas no puestas anteriormente. Como vemos en el ejemplo posterior se usa la consigna `ALTER TABLE` para alterar una tabla de una base de datos que especifiquemos. Para añadir las claves ajenas que referencien otras tablas se tienen que seguir tres pasos:

- Se le pone el nombre a la clave con `ADD CONSTRAINT`.
- Se elige el campo que queremos que sea la clave dentro de los paréntesis de `FOREIGN KEY`.
- Referenciamos con `REFERENCES` las otra tabla con su columna donde queremos que se relacione.

En este caso vemos como añadimos dos claves ajenas: *pasaporte* e *id_vuelo*.

```sql
ALTER TABLE `Iberian`.`Reservas` 
ADD CONSTRAINT `pasaporte`
  FOREIGN KEY (`pasaporte`)
  REFERENCES `Iberian`.`Pasajeros` (`pasaporte`),
ADD CONSTRAINT `id_vuelo`
  FOREIGN KEY (`id_vuelo`)
  REFERENCES `Iberian`.`Vuelos` (`id_Vuelo`);
```
A continuación vemos lo que nos hace *Workbench* cuando lo hacemos todo de forma gráfica.
```sql
ALTER TABLE `Iberian`.`Reservas` 
ADD INDEX `fk_Reservas_1_idx` (`pasaporte` ASC) VISIBLE,
ADD INDEX `id_vuelo_idx` (`id_vuelo` ASC) VISIBLE;
ALTER TABLE `Iberian`.`Reservas` 
ADD CONSTRAINT `pasaporte`
  FOREIGN KEY (`pasaporte`)
  REFERENCES `Iberian`.`Pasajeros` (`pasaporte`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `id_vuelo`
  FOREIGN KEY (`id_vuelo`)
  REFERENCES `Iberian`.`Vuelos` (`id_Vuelo`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
```

### Mostrar información base de datos
Con `SHOW` podemos mostrar información, en este caso muestra las bases de datos del sistema.
```sql
SHOW DATABASES;
```

Mientras tanto en este caso muestra el nombre de las tablas de la base de datos.
```sql
SHOW TABLES;
```

Y para concluir vemos como mostrar más en específico los datos de cada columna como puede ser el tipo de datos de cada columna de una tabla.
```sql
SHOW COLUMNS FROM Pasajeros;
```

### Inserción de datos
Se ha pedido a  *ChatGPT*<sub>[[1](#referencias)]</sub> que genere una serie de `INSERT`s para facilitar el proceso de creación de la base de datos:
```sql
-- Insertar datos de ejemplo en la tabla Pasajeros
INSERT INTO Pasajeros (pasaporte, nombre) VALUES
('ABC123', 'Juan Pérez'),
('DEF456', 'María López'),
('GHI789', 'Luis García');

-- Insertar datos de ejemplo en la tabla Vuelos
INSERT INTO Vuelos (id_Vuelo, origen, destino, fecha, capacidad) VALUES
('VUELO1', 'Madrid', 'Barcelona', '2024-04-01 08:00:00', 200),
('VUELO2', 'Barcelona', 'Madrid', '2024-04-02 10:00:00', 180),
('VUELO3', 'Sevilla', 'Valencia', '2024-04-03 12:00:00', 150);

-- Insertar datos de ejemplo en la tabla Reservas
INSERT INTO Reservas (id_Reserva, pasaporte, id_vuelo, asiento) VALUES
('RESERVA1', 'ABC123', 'VUELO1', 'A1'),
('RESERVA2', 'DEF456', 'VUELO2', 'B3'),
('RESERVA3', 'GHI789', 'VUELO3', 'C2');
```
En los `INSERT INTO` podemos ver como se especifica cada tabla y sus valores para que no haya ningún tipo de error detrás de `VALUE` entre paréntesis.
# Notas tomadas en clase
### IMPORTANTE
Cuando desarrollamos nunca empezamos de 0. siempre hay una capa por debajo, ojo, hay que conocer esa capa. Es decir, hay dedicar días para leer documentación en nuevos proyectos para evitar problemas futuros y malgasto de tiempo.

### Curiosidades
Se puede hacer una **subconsulta** para conseguir un dato de otra tabla de la siguiente manera:
```sql
DELETE FROM Reservas
WHERE pasaporte IN (
    SELECT pasaporte FROM Pasajeros
    WHERE nombre LIKE "Elsa%");
```
Es como hacer un `SELECT` dentro de un `DELETE FROM` para especificar (en este caso) los nombres que empiecen por *Elsa*.

`AUTOCOMMIT` tiene que ver con las transacciones. Esta opción sirve para que se apliquen los cambios con cada transacción/ejecución de una query, nos ayuda a no perder los datos al cerrar el programa.  
Para activarlo simplemente hay que ejecutar el siguiente código SQL:
```sql
SET AUTOCOMMIT = TRUE; -- FALSE Si no lo queremos
```
Si por el contrario queremos tener un control del progreso de la base de datos y que no se guarden los cambios cada vez que queremos realizar una transacción podemos hacerlo ejecutando el comando `COMMIT` cada vez que queramos aplicar los cambios:
```sql
COMMIT;
```
 Si queremos volver al estado en el momento del 'commiteo' ejecutamos `ROLLBACK`
```sql
ROLLBACK;
```
Obviamente deberemos desactivar el `AUTOCOMMIT` cuando queramos hacerlo manual.  
aunque Mysql no está orientado a transacciones. Lo permite pero no le pone un nombre a la transacción.  

El siguiente comando `SQL` cuenta las reservas de cada vuelo ya que los hemos agrupado con un `GROUP BY`. Esto sirve para (como bien dice el nombre) agrupar los valores del `SELECT`.
```sql
SELECT id_vuelo, COUNT(*)
FROM Reservas
GROUP BY id_vuelo;
```

Se pueden concatenar textos con los valores de los campos usando la función `CONCAT()` de la siguiente manera.
```sql
SELECT 
    *,
    YEAR(fecha),
    CONCAT("Desde: ", origen, " Hasta: ", destino)
FROM Vuelos;
```
# Respuesta a las preguntas
**¿Por qué necesitamos tres tablas?**  
Necesitamos tres tablas ya que como podemos ver en el diseño de la base de datos las tablas *pasajeros* y *vuelos* estás relacionadas de muchos a muchos, esto requiere una tabla externa como es la tabla *reservas*.  

**¿Cuáles son las claves primarias y foráneas?**  
Las claves primarias en una tabla son una forma de identificar cada registro de la tabla, los valores del campo donde esté asignada la clave no se pueden repetir. Por otro lado una clave foránea es una clave primaria de otra tabla, se usa para relacionar los datos y evitar datos erroneos como (en este caso) vuelos o pasajeros que no existan. 

**¿Por qué utilizamos las restricciones que hemos definido y no otras?**  
Hemos utilizado las restricciones `NOT NULL` para evitar valores nulos y que sea necesario rellenar esos campos. Tambíen hemos utilizado `UNSIGNED` en la tabla vuelos por optimización, esto hace que el no tome valores negativos sino que se pasen a positivo.

### Reflexiones
Para cancelar un vuelo sería necesario borrarlo de la siguiente manera:
```sql
DELETE FROM Vuelos WHERE id_Vuelo LIKE "VUELO1";
```
En este caso intentamos borrar el vuelo con id *VUELO1*. Sin embargo nos salta un error (Error: *1451*<sub>[[3](#referencias)]</sub>) que nos dice que no se puede eliminar el regitro ya que se está utilizando los datos de este registro en otra tabla (*reservas*). Si queremos borrar el registro debemos asegurarnos de que no se usa nada de información en tablas exteriores, en este caso sería borrar las reservas de ese vuelo. Se pueden denotar las ventajas e inconvenientes de las bases de datos relacionales: En primer lugar vemos que gracias a las bases de datos relacionales al relacionar la información evitamos inconcluencias en los datos almacenados y facilitamos la búsqueda de registros entre tablas. Por otro lado nos restringe a la hora de modificar o eliminar registros, ya que no nos deja eliminar un registro si está en uso en otra tabla a través de una clave foránea. 
# Referencias
[1] [Link de la herramienta ChatGPT](https://chat.openai.com/)  
[2] [Link de información sobre los integer de MySQL](https://dev.mysql.com/doc/refman/8.3/en/integer-types.html)  
[3] [Link sobre información del error 1451](https://stackoverflow.com/questions/4505624/mysql-error-1451)