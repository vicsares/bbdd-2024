CREATE DATABASE  IF NOT EXISTS `Iberian` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `Iberian`;
-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: Iberian
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Pasajeros`
--

DROP TABLE IF EXISTS `Pasajeros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Pasajeros` (
  `pasaporte` varchar(45) NOT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`pasaporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pasajeros`
--

LOCK TABLES `Pasajeros` WRITE;
/*!40000 ALTER TABLE `Pasajeros` DISABLE KEYS */;
INSERT INTO `Pasajeros` VALUES ('ABC123','Juan Pérez'),('DEF456','María López'),('GHI789','Luis García');
/*!40000 ALTER TABLE `Pasajeros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reservas`
--

DROP TABLE IF EXISTS `Reservas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Reservas` (
  `id_Reserva` varchar(10) NOT NULL,
  `pasaporte` varchar(45) NOT NULL,
  `id_vuelo` varchar(6) NOT NULL,
  `asiento` varchar(3) NOT NULL,
  PRIMARY KEY (`id_Reserva`),
  KEY `fk_Reservas_1_idx` (`pasaporte`),
  KEY `id_vuelo_idx` (`id_vuelo`),
  CONSTRAINT `id_vuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_Vuelo`),
  CONSTRAINT `pasaporte` FOREIGN KEY (`pasaporte`) REFERENCES `Pasajeros` (`pasaporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reservas`
--

LOCK TABLES `Reservas` WRITE;
/*!40000 ALTER TABLE `Reservas` DISABLE KEYS */;
INSERT INTO `Reservas` VALUES ('RESERVA1','ABC123','VUELO1','A1'),('RESERVA2','DEF456','VUELO2','B3'),('RESERVA3','GHI789','VUELO3','C2');
/*!40000 ALTER TABLE `Reservas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vuelos`
--

DROP TABLE IF EXISTS `Vuelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Vuelos` (
  `id_Vuelo` varchar(6) NOT NULL,
  `origen` varchar(45) NOT NULL,
  `destino` varchar(45) NOT NULL,
  `fecha` datetime NOT NULL,
  `capacidad` smallint unsigned NOT NULL,
  PRIMARY KEY (`id_Vuelo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vuelos`
--

LOCK TABLES `Vuelos` WRITE;
/*!40000 ALTER TABLE `Vuelos` DISABLE KEYS */;
INSERT INTO `Vuelos` VALUES ('VUELO1','Madrid','Barcelona','2024-04-01 08:00:00',200),('VUELO2','Barcelona','Madrid','2024-04-02 10:00:00',180),('VUELO3','Sevilla','Valencia','2024-04-03 12:00:00',150);
/*!40000 ALTER TABLE `Vuelos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-26 17:25:07
