# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

Reto documentado en: https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C2/Reto%202.3?ref_type=heads

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

Sinónimos: IN(), ANY(), SOME().

### Consulta 1
Obtener las canciones con una duración superior a la media.
```sql
SELECT *
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds)
                      FROM Track);
```
### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
```sql
SELECT *
FROM Invoice I
JOIN Customer C
    ON C.CustomerId = I.CustomerId
WHERE C.Email LIKE 'emma_jones@hotmail.com'
ORDER BY InvoiceDate DESC
LIMIT 5;
```

## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.
```sql
SELECT DISTINCT P.*
FROM Playlist P
JOIN PlaylistTrack PT
    ON P.PlaylistId = PT.PlaylistId
WHERE PT.TrackId IN (SELECT TrackId
                     FROM Track T
                     JOIN Chinook.Genre G
                        ON G.GenreId = T.GenreId
                     WHERE G.Name LIKE 'reggae');
```

### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20 €.
```sql
SELECT DISTINCT *
FROM Customer
WHERE CustomerId IN (SELECT CustomerId
                     FROM Invoice
                     WHERE Total > 20);
```

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.
```sql
SELECT Al.AlbumId, Al.Title, Ar.Name
FROM Album Al
JOIN Artist Ar
    ON Al.ArtistId = Ar.ArtistId
WHERE Al.AlbumId IN (SELECT AlbumId
                     FROM Track
                     GROUP BY AlbumId
                     HAVING COUNT(*) > 15);
```

### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.
```sql
SELECT AlbumId
FROM Album AS A
WHERE (SELECT COUNT(AlbumId) AS 'media'
       FROM Track AS T
       WHERE A.AlbumId = T.AlbumId)
          >
      (SELECT AVG(media)
       FROM (SELECT COUNT(T.AlbumId) AS 'media'
             FROM Track AS T
             GROUP BY T.AlbumId) AS M);
```

### Consulta 7
Obtener los álbumes con una duración total superior a la media.
```sql
SELECT AlbumId
FROM (
        SELECT AlbumId, SUM(Milliseconds) AS TotalMilliseconds
        FROM Track
        GROUP BY AlbumId
    ) AS AlbumDurations
WHERE TotalMilliseconds > (
    SELECT AVG(TotalMilliseconds)
    FROM (
        SELECT SUM(Milliseconds) AS TotalMilliseconds
        FROM Track
        GROUP BY AlbumId
    ) AS AvgAlbumDurations
);
```

### Consulta 8
Canciones del género con más canciones.
```sql
SELECT *
FROM Track
WHERE GenreId = (SELECT GenreId
                 FROM Track
                 GROUP BY GenreId
                 ORDER BY COUNT(GenreId) DESC
                 LIMIT 1);
```

### Consulta 9
Canciones de la _playlist_ con más canciones.
```sql
SELECT *
FROM Track
INNER JOIN PlaylistTrack PT on Track.TrackId = PT.TrackId
WHERE PT.PlaylistId = (SELECT PlaylistId
                       FROM PlaylistTrack
                       GROUP BY PlaylistId
                       ORDER BY COUNT(TrackId) DESC
                       LIMIT 1);
```

## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
```sql
SELECT FirstName, LastName,
    (
        SELECT SUM(Total)
        FROM Invoice AS I
        WHERE I.CustomerId = C.CustomerId
    ) AS 'Total'
FROM Customer AS C;
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
```sql
SELECT FirstName, LastName,
    (
        SELECT COUNT(CustomerId)
        FROM Customer AS C
        WHERE C.SupportRepId = E.EmployeeId
    ) AS 'Total'
FROM Employee AS E;
```

### Consulta 12
Ventas totales de cada empleado.
```sql
SELECT EmployeeId,
    (
        SELECT 
            SUM(
                (SELECT SUM(Total)
                FROM Invoice AS I
                WHERE I.CustomerId = C.CustomerId)
            ) AS 'Total'
        FROM Customer AS C
        WHERE SupportRepId = E.EmployeeId
        GROUP BY SupportRepId
    )
FROM Employee E
```

### Consulta 13
Álbumes junto al número de canciones en cada uno.
```sql
SELECT A.AlbumId, A.Title,
    (
        SELECT COUNT(AlbumId)
        FROM Track AS T
        WHERE T.AlbumId = A.AlbumId
    ) AS 'Número de canciones'
FROM Album AS A;
```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.
```sql
SELECT ArtistId, Name, Antigua
    FROM (
        SELECT Ar.ArtistId, Ar.Name,
            (
                SELECT Title
                FROM Album AS Al
                WHERE Al.ArtistId = Ar.ArtistId
                ORDER BY Al.AlbumId
                LIMIT 1
            ) AS Antigua
        FROM Artist AS Ar
    ) AS Subconsulta
WHERE Antigua IS NOT NULL;

```

## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/


<!--
HAVING ??
GROUP BY ??
-->
