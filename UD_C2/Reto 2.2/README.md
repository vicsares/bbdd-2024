# Reto 2.2: Consultas con subconsultas
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Consultas con subconsultas

### Consulta 1
Muestra las listas de reproducción cuyo nombre comienza por `M`, junto a las 3 primeras canciones de cada uno, ordenadas por álbum y por precio (más bajo primero).
```sql
SELECT
    p.Name AS PlaylistName,
    t.TrackId,
    t.Name AS TrackName,
    a.Title AS AlbumTitle,
    t.UnitPrice
FROM
    Playlist p
        JOIN
    PlaylistTrack pt ON p.PlaylistId = pt.PlaylistId
        JOIN
    Track t ON pt.TrackId = t.TrackId
        JOIN
    Album a ON t.AlbumId = a.AlbumId
WHERE
    p.Name LIKE 'M%'
  AND t.TrackId IN (
    SELECT
        TrackId
    FROM
        PlaylistTrack
    WHERE
        PlaylistId = p.PlaylistId
    ORDER BY
        TrackId
    LIMIT 3
)
ORDER BY
    AlbumTitle,
    UnitPrice;
```

### Consulta 2
Muestra todos los artistas que tienen canciones con duración superior a 5 minutos.
```sql
SELECT ar.Name AS ArtistName
FROM Artist ar
WHERE EXISTS (
    SELECT 1
    FROM Album al
             JOIN Track t ON al.AlbumId = t.AlbumId
    WHERE al.ArtistId = ar.ArtistId
      AND t.Milliseconds > 300000
);
```

### Consulta 3
Muestra nombre y apellidos de los empleados que tienen clientes asignados.
(Pista: se puede hacer con `EXISTS`).
```sql
SELECT e.FirstName, e.LastName
FROM Employee e
WHERE EXISTS (
    SELECT 1
    FROM Customer c
    WHERE c.SupportRepId = e.EmployeeId
);
```

### Consulta 4
Muestra todas las canciones que no han sido compradas.
```sql
SELECT t.TrackId, t.Name AS TrackName
FROM Track t
WHERE NOT EXISTS (
    SELECT 1
    FROM InvoiceLine il
    WHERE il.TrackId = t.TrackId
);
```

### Consulta 5
Lista los empleados junto a sus subordinados (empleados que reportan a ellos).
```sql
SELECT 
    manager.EmployeeId AS ManagerId,
    manager.FirstName AS ManagerFirstName,
    manager.LastName AS ManagerLastName,
    subordinate.EmployeeId AS SubordinateId,
    subordinate.FirstName AS SubordinateFirstName,
    subordinate.LastName AS SubordinateLastName
FROM 
    Employee manager
JOIN 
    Employee subordinate ON manager.EmployeeId = subordinate.ReportsTo
ORDER BY 
    manager.EmployeeId, subordinate.EmployeeId;
```

### Consulta 6
Muestra todas las canciones que ha comprado el cliente Luis Rojas.
```sql
SELECT t.TrackId, t.Name AS TrackName, t.Composer, t.UnitPrice
FROM Customer c
JOIN Invoice i ON c.CustomerId = i.CustomerId
JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId
JOIN Track t ON il.TrackId = t.TrackId
WHERE c.FirstName = 'Luis' AND c.LastName = 'Rojas'
ORDER BY t.Name;
```

### Consulta 7
Canciones que son más caras que cualquier otra canción del mismo álbum.
```sql
SELECT 
    t.TrackId, 
    t.Name AS TrackName, 
    t.UnitPrice, 
    a.Title AS AlbumTitle
FROM 
    Track t
JOIN 
    Album a ON t.AlbumId = a.AlbumId
WHERE 
    t.UnitPrice > (
        SELECT MAX(t2.UnitPrice)
        FROM Track t2
        WHERE t2.AlbumId = t.AlbumId
    )
ORDER BY 
    a.Title, t.UnitPrice DESC;
```

### Consulta 7.2
Muestra la cación más larga de cada álbum.
```sql
SELECT 
    t.TrackId, 
    t.Name AS TrackName, 
    t.Milliseconds AS DurationMillis,
    t.AlbumId,
    a.Title AS AlbumTitle
FROM 
    Track t
JOIN 
    Album a ON t.AlbumId = a.AlbumId
JOIN (
    SELECT 
        AlbumId,
        MAX(Milliseconds) AS MaxDuration
    FROM 
        Track
    GROUP BY 
        AlbumId
) AS max_duration_per_album ON t.AlbumId = max_duration_per_album.AlbumId 
                             AND t.Milliseconds = max_duration_per_album.MaxDuration
ORDER BY 
    a.Title;
```

### Consulta 7.3
Muestra la cación más larga de cada factura.
```sql
SELECT 
    i.InvoiceId,
    t.TrackId,
    t.Name AS TrackName,
    t.Milliseconds AS DurationMillis
FROM 
    Invoice i
JOIN 
    InvoiceLine il ON i.InvoiceId = il.InvoiceId
JOIN 
    Track t ON il.TrackId = t.TrackId
JOIN (
    SELECT 
        il.InvoiceId,
        MAX(t.Milliseconds) AS MaxDuration
    FROM 
        InvoiceLine il
    JOIN 
        Track t ON il.TrackId = t.TrackId
    GROUP BY 
        il.InvoiceId
) AS max_duration_per_invoice ON i.InvoiceId = max_duration_per_invoice.InvoiceId 
                                 AND t.Milliseconds = max_duration_per_invoice.MaxDuration
ORDER BY 
    i.InvoiceId;
```

## Consultas con subconsultas y funciones de agregación

### Consulta 8
Clientes que han comprado todos los álbumes de _Queen_.
```sql
SELECT 
    c.CustomerId, 
    c.FirstName, 
    c.LastName
FROM 
    Customer c
WHERE 
    NOT EXISTS (
        SELECT 
            a.AlbumId
        FROM 
            Album a
        JOIN 
            Artist ar ON a.ArtistId = ar.ArtistId
        WHERE 
            ar.Name = 'Queen'
            AND NOT EXISTS (
                SELECT 
                    t.TrackId
                FROM 
                    Track t
                LEFT JOIN 
                    InvoiceLine il ON t.TrackId = il.TrackId
                WHERE 
                    t.AlbumId = a.AlbumId
                    AND il.InvoiceId IN (
                        SELECT 
                            i.InvoiceId
                        FROM 
                            Invoice i
                        WHERE 
                            i.CustomerId = c.CustomerId
                    )
            )
    );
```

### Consulta 9
Clientes que han comprado más de 10 canciones en una sola transacción.
```sql
SELECT 
    c.CustomerId, 
    c.FirstName, 
    c.LastName
FROM 
    Customer c
JOIN 
    Invoice i ON c.CustomerId = i.CustomerId
JOIN (
    SELECT 
        InvoiceId, 
        COUNT(*) AS NumTracks
    FROM 
        InvoiceLine
    GROUP BY 
        InvoiceId
    HAVING 
        COUNT(*) > 10
) AS NumTracksPerInvoice ON i.InvoiceId = NumTracksPerInvoice.InvoiceId;
```

### Consulta 10
Muestra las 10 canciones más compradas.
```sql
SELECT 
    t.TrackId,
    t.Name AS TrackName,
    COUNT(il.TrackId) AS NumPurchases
FROM 
    Track t
JOIN 
    InvoiceLine il ON t.TrackId = il.TrackId
GROUP BY 
    t.TrackId
ORDER BY 
    NumPurchases DESC
LIMIT 10;
```

### Consulta 11
Muestra las canciones con una duración superior a la media.
```sql
SELECT 
    t.TrackId,
    t.Name AS TrackName,
    t.Milliseconds AS DurationMillis
FROM 
    Track t
WHERE 
    t.Milliseconds > (
        SELECT 
            AVG(t2.Milliseconds)
        FROM 
            Track t2
    );
```

### Consulta 12
Para demostrar lo bueno que es nuestro servicio, muestra el número de países donde tenemos clientes, el número de géneros músicales de los que disponemos y el número de pistas.
```sql
SELECT COUNT(DISTINCT Country) AS NumCountries FROM Customer;
```
```sql
SELECT COUNT(DISTINCT GenreId) AS NumGenres FROM Genre;
```
```sql
SELECT COUNT(*) AS NumTracks FROM Track;
```

### Consulta 13
Canciones vendidas más que la media de ventas por canción.
```sql
SELECT 
    t.TrackId,
    t.Name AS TrackName,
    COUNT(il.InvoiceLineId) AS SalesCount
FROM 
    Track t
JOIN 
    InvoiceLine il ON t.TrackId = il.TrackId
GROUP BY 
    t.TrackId
HAVING 
    COUNT(il.InvoiceLineId) > (
        SELECT 
            AVG(sales_count)
        FROM (
            SELECT 
                COUNT(il2.InvoiceLineId) AS sales_count
            FROM 
                InvoiceLine il2
            GROUP BY 
                il2.TrackId
        ) AS avg_sales_per_track
    );
```

## Consultas con casos

### Consulta 14
Clasifica a los clientes en tres categorías según su país: "Local" si el país es 'USA', "Internacional" si el país es distinto de 'USA', y "Desconocido" si el país es nulo.
```sql
SELECT 
    CustomerId,
    FirstName,
    LastName,
    Country,
    CASE 
        WHEN Country IS NULL THEN 'Desconocido'
        WHEN Country = 'USA' THEN 'Local'
        ELSE 'Internacional'
    END AS Categoria
FROM 
    Customer;
```

### Consulta 15
Calcula el descuento aplicado sobre cada factura, que depende del monto total de la factura (10% para facturas superiores a $10).
```sql
SELECT 
    InvoiceId,
    Total,
    IF(Total > 10, Total * 0.1, 0) AS Descuento
FROM 
    Invoice;
```

### Consulta 16
Clasifica a los empleados en diferentes niveles según su cargo (manager, asistente o empleado).
```sql
SELECT 
    EmployeeId,
    FirstName,
    LastName,
    Title,
    CASE 
        WHEN Title = 'General Manager' THEN 'Manager'
        WHEN Title LIKE '%Assistant%' THEN 'Asistente'
        ELSE 'Empleado'
    END AS Nivel
FROM 
    Employee;
```

### Consulta 17
Etiqueta a los clientes como "VIP" si han gastado más de $45 en compras totales.
```sql
SELECT 
    CustomerId,
    FirstName,
    LastName,
    TotalSpent,
    CASE 
        WHEN TotalSpent > 45 THEN 'VIP'
        ELSE 'Regular'
    END AS CustomerType
FROM 
    (SELECT 
        c.CustomerId,
        c.FirstName,
        c.LastName,
        SUM(i.Total) AS TotalSpent
    FROM 
        Customer c
    JOIN 
        Invoice i ON c.CustomerId = i.CustomerId
    GROUP BY 
        c.CustomerId) AS CustomerTotalSpent;
```
