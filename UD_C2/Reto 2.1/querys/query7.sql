-- query7
SELECT *
FROM Artist AS Ar
LEFT JOIN Album AS Al
    ON Ar.ArtistId = Al.ArtistId;