-- query 16
SELECT G.name AS 'Nombre',
       COUNT(G.GenreId) AS 'Número de canciones'
FROM Track AS T
JOIN Genre AS G
    ON T.GenreId = G.GenreId
GROUP BY T.GenreId;