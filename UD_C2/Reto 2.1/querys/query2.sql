-- query2
SELECT InvoiceId AS 'IdFactura', InvoiceDate AS 'FechaFactura', Total
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(CURRENT_DATE()) AND MONTH(InvoiceDate) <= 3;