-- query19
SELECT A.Title AS 'Título',
       COUNT(A.Title) AS 'Ventas'
FROM Album AS A
JOIN Track AS T
    ON A.AlbumId = T.AlbumId
JOIN InvoiceLine AS IL
    ON T.TrackId = IL.TrackId
GROUP BY A.Title
ORDER BY Ventas DESC
LIMIT 6;