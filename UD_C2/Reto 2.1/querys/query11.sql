-- query11
SELECT C.CustomerId AS 'IdCliente',
       C.FirstName AS 'Nombre',
       C.LastName AS 'Apellido',
       I.InvoiceId AS 'IdFactura',
       I.CustomerId AS 'IdCliente',
       I.Total AS 'Precio'
FROM Customer AS C
JOIN Invoice AS I
    ON C.CustomerId = I.CustomerId
WHERE I.Total > 10
ORDER BY C.LastName ASC;