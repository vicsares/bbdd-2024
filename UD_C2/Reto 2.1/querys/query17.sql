-- query17
SELECT A.Title AS 'Título',
       COUNT(A.Title) AS 'Número de canciones'
FROM Album AS A
JOIN Track AS T
    ON A.AlbumId = T.AlbumId
GROUP BY A.Title;