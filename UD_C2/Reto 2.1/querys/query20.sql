-- query20
SELECT Country, COUNT(Country)
FROM Customer
GROUP BY Country
HAVING COUNT(Country) >= 5;