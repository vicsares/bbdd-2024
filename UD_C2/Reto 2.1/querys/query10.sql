-- query10
SELECT P.PlaylistId,
       P.NAme AS 'Nombre',
       PT.PlaylistId,
       PT.TrackId,
       T.TrackId AS 'IdCanción',
       T.Name AS 'Nombre Canción',
       T.Milliseconds AS 'Milisegundos'
FROM Playlist AS P
JOIN PlaylistTrack AS PT
    ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T
    ON PT.TrackId = T.TrackId
WHERE P.Name LIKE 'C%'
ORDER BY P.PlaylistId DESC, T.Milliseconds DESC;