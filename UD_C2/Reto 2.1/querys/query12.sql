-- query12
SELECT AVG(DISTINCT Total) AS 'Media',
       MAX(DISTINCT Total) AS 'Máximo',
       MIN(DISTINCT Total) AS 'Mínimo'
FROM Invoice;