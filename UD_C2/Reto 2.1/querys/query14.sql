-- query14
SELECT A.Title, COUNT(TrackId) AS 'Número de canciones'
FROM Track AS T
JOIN Album AS A
    ON T.AlbumId = A.AlbumId
WHERE A.Title LIKE 'Out Of Time'
GROUP BY A.Title;