-- query 9
SELECT I.InvoiceDate AS 'Fecha factura',
       C.LastName AS 'Apellido',
       C.FirstName AS 'Nombre',
       I.BillingAddress AS 'Dirección de facturación',
       I.BillingPostalCode AS 'Código postal',
       I.BillingCountry AS 'País',
       I.Total AS 'Importe'
FROM Invoice AS I
JOIN Customer AS C
    ON I.CustomerId = C.CustomerId
WHERE I.BillingCity LIKE 'Berlin';