-- query4
SELECT TrackId AS 'IdCanción', Name AS 'Nombre', Bytes
FROM Track
ORDER BY Bytes DESC
LIMIT 10;