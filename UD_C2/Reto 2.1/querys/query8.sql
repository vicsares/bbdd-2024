-- query8
SELECT E1.EmployeeId AS 'IdEmpleado',
       E1.LastName AS 'Apellido',
       E1.FirstName AS 'Nombre',
       E1.BirthDate AS 'Fecha cumpleaños',
       E2.EmployeeId AS 'IdSupervisor',
       E2.LastName AS 'Apellido supervisor',
       E2.FirstName AS 'Nombre supervisor'
FROM Employee AS E1
JOIN Employee AS E2
    ON E1.EmployeeId = E2.ReportsTo
ORDER BY E1.BirthDate DESC
LIMIT 15;