-- query18
SELECT G.Name AS 'Nombre',
       COUNT(G.GenreId) AS 'Ventas'
FROM Genre AS G
JOIN Track AS T
    ON G.GenreId = T.GenreId
JOIN InvoiceLine AS IL
    ON T.TrackId = IL.TrackId
GROUP BY G.GenreId
ORDER BY Ventas DESC
LIMIT 10;