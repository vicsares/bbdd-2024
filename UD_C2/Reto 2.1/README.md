# Reto 2.1: Consultas con funciones de agregación

En este reto vamos a trabajar con una base de datos descargada de internet, esta base de datos se llama Chinook<sub>[[1](#referencias)]</sub> y también se puede encontrar en este apartado de gitlab en el fichero Chinook_MySql.sql.
Vamos a realizar una serie de consultas recapitulando sobre los `JOIN`s, algunas funciones relacionadas con las fechas, ordenamiento de resultado, limitación de datos, etc. También veremos las funciones de agregación que se explicarán más adelante.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C2/Reto%202.1?ref_type=heads

A continuación se mostrará el diagrama relacional de Chinook:  
![Diagrama relacional de Chinook](esquema.png)

## Consultas sobre una tabla
### Query 1
**Encuentra todos los clientes de Francia.**  
Para encontrar todos los clientes de una tabla en específico ya hemos visto en retos anteriores que podemos usar un `SELECT` común con una restricción `WHERE` para especificar (en este caso) el país pedido.
```sql
-- query1
SELECT *
FROM Customer
WHERE Country LIKE "France";
```

### Query 2
**Muestra las facturas del primer trimestre de este año.**  
Para esta query hemos utilizado más funciones aunque ninguna desconocida ya vista. Hemos aplicado un alias a cada campo que se buscaba mostrar con `AS` y en el `WHERE` hemos utilizado 3 diferentes funciones relacionadas con fechas. Para empezar hemos igualado el año al actual para que únicamente nos muestre facturas de este año con `YEAR` que selecciona el año de una fecha y `CURRENT_DATE` que se usa para obtener la fecha actual. Después hemos utilizado un `AND` para anidar condiciones y como buscamos los del primer trimestre de este año simplemente se ha buscado los meses menores a 3.
```sql
-- query2
SELECT InvoiceId AS 'IdFactura', InvoiceDate AS 'FechaFactura', Total
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(CURRENT_DATE()) AND MONTH(InvoiceDate) <= 3;
```

### Query 3
**Muestra todas las canciones compuestas por AC/DC.**  
Para este reto tampoco ha aparecido nada novedoso. Hemos utilizado alias con `AS` y en el `WHERE` ya que buscamos comparar cadenas se usa un `LIKE` para buscar las canciones de 'AC/DC'.
```sql
-- query3
SELECT TrackId As 'IdCanción', Name AS 'Nombre', Composer AS 'Grupo'
FROM Chinook.Track
WHERE Composer LIKE 'AC/DC';
```

### Query 4
**Muestra las 10 canciones que más tamaño ocupan.**  
En esta query hemos utilizado dos funciones para poder sacar las 10 canciones que más tamaño ocupan. En primer lugar, hemos utilizado `ORDER BY` para ordenar el resultado de datos y con `DESC` especificamos que sea en orden descendente. Después hemos usado `LIMIT` para limitar la salida de datos (en este caso) a únicamente 10 registros.
```sql
-- query4
SELECT TrackId AS 'IdCanción', Name AS 'Nombre', Bytes 
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```

### Query 5
**Muestra el nombre de aquellos países en los que tenemos clientes.**  
Para este reto únicamente hemos utilizado `DISCTINC` para evitar que se repitan ciudades que ya salen en la salida de datos.
```sql
-- query5
SELECT DISTINCT City AS 'Ciudad'
FROM Customer;
```

### Query 6
**Muestra todos los géneros musicales.**  
Esta sin dudarlo es la consulta más básica, ya que es un `SELECT` común y corriente de la tabla 'Genre'.
```sql
-- query6
SELECT * 
FROM Genre;
```

## Consultas sobre múltiples tablas
Aquí es donde ya empezamos a utilizar `JOIN`s para poder seleccionar campos de diferentes tablas en un mismo `SELECT`. Los `JOIN`s ya fueron explicados en el reto3 de la UD_C1<sub>[[2](#referencias)]</sub>

### Query 7
**Muestra todos los artistas junto a sus álbumes.**  
En este reto hemos utilizado un `LEFT JOIN` ya que es posible que hallan artistas que todavía no tengan un album asociado o simplemente no tengan un album creado.
```sql
-- query7
SELECT *
FROM Artist AS Ar 
LEFT JOIN Album AS Al
    ON Ar.ArtistId = Al.ArtistId;
```

### Query 8
**Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.**  
Para este otro reto hemos utilizado un `SELF JOIN` ya que podemos apreciar como el `JOIN` va dirigido a la misma tabla relacionando campos diferentes, porque los supervisores están en la misma tabla de empleados. En este caso es necesario distinguir las tablas con alias. Después hemos usado `ORDER BY` y `LIMIT` para poder listar los 15 empleados más jóvenes.
```sql
-- query8
SELECT E1.EmployeeId AS 'IdEmpleado',
       E1.LastName AS 'Apellido',
       E1.FirstName AS 'Nombre',
       E1.BirthDate AS 'Fecha cumpleaños',
       E2.EmployeeId AS 'IdSupervisor',
       E2.LastName AS 'Apellido supervisor',
       E2.FirstName AS 'Nombre supervisor'
FROM Employee AS E1
JOIN Employee AS E2
    ON E1.EmployeeId = E2.ReportsTo
ORDER BY E1.BirthDate DESC
LIMIT 15;
```

### Query 9
**Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden).**  
En este reto no hemos hecho nada que no se haya visto anteriormente e incluso en este reto así que no voy a extenderme mucho en esta query.
```sql
-- query 9
SELECT I.InvoiceDate AS 'Fecha factura',
       C.LastName AS 'Apellido',
       C.FirstName AS 'Nombre',
       I.BillingAddress AS 'Dirección de facturación',
       I.BillingPostalCode AS 'Código postal',
       I.BillingCountry AS 'País',
       I.Total AS 'Importe'
FROM Invoice AS I
JOIN Customer AS C
    ON I.CustomerId = C.CustomerId
WHERE I.BillingCity LIKE 'Berlin';
```

### Query 10
**Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.**  
En esta query hemos utilizado un doble `JOIN` ya que no hay ninguna relación directa entre las tablas 'Playlist' y 'Track'. Vemos como hay que pasar por la tabla intermedia 'PlaylistTrack' es por ello que ha hecho falta el doble `JOIN`. También hemos utilizado una _wildcard_ para poder encontrar los nombres que empiecen por 'C' (hemos empleado la _wildcard_ '%'). Para finalizar podemos observar de forma curiosa que hay también hay un doble `ORDER BY`, lo ordena básicamente con el orden en el que le has puesto los campos separados por una coma. 
```sql
-- query10
SELECT P.PlaylistId, 
       P.NAme AS 'Nombre', 
       PT.PlaylistId,
       PT.TrackId, 
       T.TrackId AS 'IdCanción', 
       T.Name AS 'Nombre Canción', 
       T.Milliseconds AS 'Milisegundos'
FROM Playlist AS P
JOIN PlaylistTrack AS PT
    ON P.PlaylistId = PT.PlaylistId
JOIN Track AS T
    ON PT.TrackId = T.TrackId
WHERE P.Name LIKE 'C%'
ORDER BY P.PlaylistId DESC, T.Milliseconds DESC;
```

### Query 11
**Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.**  
En esta query tampoco hemos utilizado nada nuevo, simplemente se buscaba los clientes con compras mayores a 10€ y con un simple `JOIN` se ha conseguido. Nos podemos fijar que `ORDER BY` también ordena por orden alfabético.
```sql
-- query11
SELECT C.CustomerId AS 'IdCliente',
       C.FirstName AS 'Nombre',
       C.LastName AS 'Apellido',
       I.InvoiceId AS 'IdFactura',
       I.CustomerId AS 'IdCliente',
       I.Total AS 'Precio'
FROM Customer AS C
JOIN Invoice AS I
    ON C.CustomerId = I.CustomerId
WHERE I.Total > 10
ORDER BY C.LastName ASC;
```

## Consultas con funciones de agregación
Resumidamente, las funciones de agregación son aquellas en las que se crea una tabla nueva en base a otra tabla. Se suelen usar para operaciones matemáticas como veremos más adelante.

### Query 12
**Muestra el importe medio, mínimo y máximo de cada factura.**  
En este reto vemos como hemos utilizado tres nuevas funciones. En primer lugar, hemos utilizado `AVG` que su función es calcular la media de los valores obtenidos y mostrarlo en forma de campo. Seguidamente, hemos utilizado `MAX` que resulta bastante descriptivo, esta función devuelve el valor máximo de los valores obtenidos. Para finalizar, hemos usado también `MIN` que resulta lo contrario de `MAX` siendo también muy descriptivo, ya que devuelve el valor mínimo.
```sql
-- query12
SELECT AVG(DISTINCT Total) AS 'Media',
       MAX(DISTINCT Total) AS 'Máximo',
       MIN(DISTINCT Total) AS 'Mínimo'
FROM Invoice;
```

### Query 13
**Muestra el número total de artistas.**  
Esta query se ha podido realizar con y sin funciones de agregación ya que si nos fijamos los artistas están enumerados por un ID y simplemente mostrado el último ID podríamos obtener el número de artistas como se ve a continuación.
```sql
-- query 13.5
SELECT ArtistId AS Último
FROM Artist
ORDER BY ArtistId DESC
LIMIT 1;
```
Sin embargo, esta sección es de funciones de agregación y se busca usar nuevas funciones así que hemos usado también la función `COUNT` que cuenta el número de registros que devuelve el `SELECT`.
```sql
-- query 13
SELECT COUNT(ArtistId) AS Último
FROM Artist;
```

### Query 14
**Muestra el número de canciones del álbum “Out Of Time”.**  
Esta query es muy curiosa, ya que hemos utilizado `GROUP BY`, esta cláusula necesita de funciones de agregación para poder usarse, ya que agrupa grupos de datos en específico.
```sql
-- query14
SELECT A.Title, COUNT(TrackId) AS 'Número de canciones'
FROM Track AS T
JOIN Album AS A
    ON T.AlbumId = A.AlbumId
WHERE A.Title LIKE 'Out Of Time'
GROUP BY A.Title;
```
Investigando con compañeros también vemos que es posible con otra cláusula, este caso `HAVING`. Maneja datos con `GROUP BY` ya que mientras que la cláusula `WHERE` se utiliza para filtrar filas individuales antes de que se agrupen, la cláusula `HAVING` se utiliza para filtrar grupos de filas después de que se hayan agrupado.
```sql
-- query14.5
SELECT A.Title, COUNT(TrackId) AS 'Número de canciones'
FROM Track AS T 
JOIN Album AS A
    ON T.AlbumId = A.AlbumId
GROUP BY A.Title
HAVING A.Title LIKE 'Out Of Time';
```

### Query 15
**Muestra el número de países donde tenemos clientes.**  
En esta simplemente hemos utilizado `COUNT` que cuenta el número de registros (es lo mismo que la query 5 pero con un `COUNT`).
```sql
-- query15
SELECT COUNT(DISTINCT Country) AS 'Número de países'
FROM Customer;
```

### Query 16
**Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).**  
Hemos notado que para poder mostrar más de un campo con `COUNT` debemos usar `GROUP BY`. Es por eso que no se ha usado en el reto anterior pero en este sí.
```sql
-- query 16
SELECT G.name AS 'Nombre',
       COUNT(G.GenreId) AS 'Número de canciones'
FROM Track AS T
JOIN Genre AS G
    ON T.GenreId = G.GenreId
GROUP BY T.GenreId;
```

### Query 17
**Muestra los álbumes ordenados por el número de canciones que tiene cada uno.**  
En esta query no hemos visto nada que no se haya visto antes. Se ha usado `COUNT` con un `GROUP BY` para poder mostrar varios campos aunque esté el `COUNT`.
```sql
-- query17
SELECT A.Title AS 'Título',
       COUNT(A.Title) AS 'Número de canciones'
FROM Album AS A
JOIN Track AS T
    ON A.AlbumId = T.AlbumId
GROUP BY A.Title;
```

### Query 18
**Encuentra los géneros musicales más populares (los más comprados).**  
Para este reto tampoco se ha usado nada nuevo. Se puede ver como se ha usado un doble `JOIN` para poder relacionar las tablas de 'Genre' con la de 'InvoiceLine', ya que no tienen una conexión directa.
```sql
-- query18
SELECT G.Name AS 'Nombre', 
       COUNT(G.GenreId) AS 'Ventas'
FROM Genre AS G 
JOIN Track AS T
    ON G.GenreId = T.GenreId
JOIN InvoiceLine AS IL
    ON T.TrackId = IL.TrackId
GROUP BY G.GenreId
ORDER BY Ventas DESC
LIMIT 10;
```

### Query 19
**Lista los 6 álbumes que acumulan más compras.**  
En esta query es prácticamente lo mismo que la anterior simplemente cambiando los datos, ya que las tablas 'Album' y 'InvoiceLine' no tienen una conexión directa entre ellas.
```sql
-- query19
SELECT A.Title AS 'Título', 
       COUNT(A.Title) AS 'Ventas'
FROM Album AS A 
JOIN Track AS T
    ON A.AlbumId = T.AlbumId
JOIN InvoiceLine AS IL
    ON T.TrackId = IL.TrackId
GROUP BY A.Title
ORDER BY Ventas DESC
LIMIT 6;
```

### Query 20
**Muestra los países en los que tenemos al menos 5 clientes**  
Por último en esta query se ha vuelto a utilizar el `HAVING` ya que con `WHERE` no podemos hacer la comprobación porque se necesita hacerlo después de agrupar los datos.
```sql
-- query20
SELECT Country, COUNT(Country)
FROM Customer
GROUP BY Country
HAVING COUNT(Country) >= 5;
```

# Referencias
[1] [Link del github de donde se ha sacado la base de datos Chinook](https://github.com/lerocha/chinook-database)  
[2] [Link al reto 3 de la UD_C1](https://gitlab.com/vicsares/bbdd-2024/-/tree/main/UD_C1/reto3?ref_type=heads)